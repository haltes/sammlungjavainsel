package com.tutego.insel.solutions.util;

import java.util.*;

public class Keller {
  private LinkedList<Object> list = new LinkedList<>();

  public boolean isEmpty() {
    return list.isEmpty();
  }

  public Object pop() {
    if ( !isEmpty() )
      return list.removeLast();
    return null;
  }

  public void push( Object o ) {
    list.addLast( o );
  }

  /**
   * Eine kleine Testfunktion
   */
  public static void main( String[] args ) {
    Keller k = new Keller();

    k.push( "nett" );
    k.push( "bist" );
    k.pop();
    k.push( "Du" );
    System.out.println( k.pop() );
    System.out.println( k.isEmpty() );
    System.out.println( k.pop() );
    System.out.println( k.isEmpty() );
  }
}
package com.tutego.insel.solutions.thread.fruchtigsahnig;

public class MyApplication {

  public static void main( String[] args ) {
    Schreiber schr = new Schreiber();
    Geschmack sahne = new Geschmack( 500, "sahnig", schr );
    Geschmack frucht = new Geschmack( 1500, "fruchtig", schr );

    Thread sahneThread = new Thread( sahne );
    Thread fruchtThread = new Thread( frucht );
    sahneThread.start();
    fruchtThread.start();
  }
}
package com.tutego.insel.solutions.ui.swing;

import java.awt.BorderLayout;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;

public class ProgressFrame extends JFrame
{
  private JButton button;
  private JTextArea text;

  public ProgressFrame()
  {
    JPanel panel = new JPanel( new BorderLayout( 6, 6 ) );
    panel.setBorder( BorderFactory.createEmptyBorder( 6, 6, 6, 6 ) );

    button = new JButton( "Laden" );
    button.addActionListener( new ActionListener()
    {
      @Override
      public void actionPerformed( ActionEvent e )
      {
        button.setEnabled( false );
        // setCursor( JFrame.WAIT_CURSOR );

        Runnable run = new Runnable()
        {
          @Override
          public void run()
          {
            ProgressFrame.this.choosedButton();
          }
        };
        new Thread( run ).start();
      }
    } );

    text = new JTextArea( 20, 80 );
    text.setEditable( false );

    panel.add( button, BorderLayout.NORTH );
    panel.add( new JScrollPane( text ), BorderLayout.CENTER );

    this.getContentPane().add( panel );
    this.pack();
  }

  private void choosedButton()
  {
    StringBuilder textbuffer = new StringBuilder();
    text.setText( "" );

    try
    {
      InputStream in = new FileInputStream( "readme.html" );
      ProgressMonitorInputStream pmin = new ProgressMonitorInputStream( null,
                                                                        "laden...",
                                                                        in );
      BufferedReader br = new BufferedReader( new InputStreamReader( pmin ) );

      int nr = 1;
      
      String line;
      while ( (line = br.readLine()) != null )
      {
        textbuffer.append( getLineNr( nr++ ) );
        textbuffer.append( line );
        textbuffer.append( '\n' );
      }
    }
    catch ( Exception e )
    {
      if ( e instanceof InterruptedIOException )
        textbuffer.append( "Laden abgebrochen!" );
      else
        e.printStackTrace();
    }
    text.setText( textbuffer.toString() );

    button.setEnabled( true );
    // setCursor( JFrame.DEFAULT_CURSOR );
  }

  private String getLineNr( int nr )
  {
    String res = "00000" + nr + ":";

    return res.substring( res.length() - 6, res.length() );
  }
}
package com.tutego.insel.solutions.thread.fruchtigsahnig;

public class Geschmack implements Runnable {

  private int delay;
  private String str;
  private Schreiber schr;

  public Geschmack( int delay, String str, Schreiber schr ) {
    this.delay = delay;
    this.str = str;
    this.schr = schr;
  }

  @Override
  public void run() {
    System.out.println( "Start: " + str );
    while ( true ) {
      try {
        Thread.sleep( delay );
      }
      catch ( Exception e ) {
        System.out.println( e );
        System.exit( 1 );
      }
      schr.schreibe( str );
    }
  }
}
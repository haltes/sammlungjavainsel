package com.tutego.insel.solutions.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UseDatabaseUtility
{
  public static void main( String[] args ) throws SQLException
  {
    DatabaseUtility db = new DatabaseUtility();
    
    Statement stmt = db.getStatement();

    String s = "SELECT * FROM geodb_adm0";
    ResultSet rSet = stmt.executeQuery( s );

    while ( rSet.next() )
    {
      System.out.println ( rSet.getString(1) + "\t" +
                           rSet.getString(2) );
    }
    
    rSet.close();
    stmt.close();
  }
}

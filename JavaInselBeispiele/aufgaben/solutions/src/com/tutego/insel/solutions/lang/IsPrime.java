package com.tutego.insel.solutions.lang;

class IsPrime {
  public static void main( String[] args ) {
    int n;

    n = 57;
    System.out.println( isPrime( n ) ? "Primzahl" : "Keine Primzahl" );

    n = 58;
    System.out.println( isPrime( n ) ? "Primzahl" : "Keine Primzahl" );

    n = 59;
    System.out.println( isPrime( n ) ? "Primzahl" : "Keine Primzahl" );

    printFirstPrimes( 100 );
  }

  static boolean isPrime( int n ) {
    int mod = 1;

    for ( int i = 2, end = (int) Math.sqrt( n ); i <= end && mod != 0; i++ )
      mod = n % i;

    return (mod != 0);
  }

  static void printFirstPrimes( int noOfPrimes ) {
    for ( int j = 2, i = 0; i < noOfPrimes; j++ )
      if ( isPrime( j ) ) {
        System.out.println( j + " ist Primzahl" );
        i++;
      }
  }
}
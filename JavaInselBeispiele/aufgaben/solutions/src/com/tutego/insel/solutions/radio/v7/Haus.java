package com.tutego.insel.solutions.radio.v7;

import java.util.ArrayList;

public class Haus {

  private ArrayList<Konsumgeraet> geräteImHaus = new ArrayList<>();

  public void add( Konsumgeraet g ) {
      geräteImHaus.add( g );
  }

  public void urlaub() {
    for ( Konsumgeraet g : geräteImHaus ) {
      g.aus();
    }
  }

  public static void main( String[] args ) {
    Radio omisAltesRadio = new Radio();
    omisAltesRadio.an();
    omisAltesRadio.lauter();
    omisAltesRadio.lauter();
    System.out.println( omisAltesRadio.toString() );
    omisAltesRadio.aus();

    Haus h = new Haus();
    h.add( omisAltesRadio );
    h.add( new Fernsehen() );
    h.add( new Konsumgeraet() );
    h.urlaub();
  }
}
package com.tutego.insel.solutions.radio.v6;

import java.util.ArrayList;

class Haus {
  private ArrayList<Radio> radios = new ArrayList<>();

  void addRadio( Radio radio ) {
    radios.add( radio );
  }

  void listeRadios() {
    for ( Radio r : radios ) {
      System.out.println( r );
    }
  }
}

class Radio {
  @Override
  public String toString() {
    return "Ich bin ein Radio";
  }
}

public class WohnsiedlungMitRadios {

  public static void main( String[] args ) {
    Haus h1 = new Haus();
    // Haus h2 = new Haus();

    Radio r1 = new Radio();
    Radio r2 = new Radio();

    h1.addRadio( r1 );
    h1.addRadio( r2 );

    h1.listeRadios();
  }
}

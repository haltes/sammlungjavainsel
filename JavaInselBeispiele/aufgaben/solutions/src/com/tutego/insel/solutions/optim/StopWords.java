package com.tutego.insel.solutions.optim;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;

public class StopWords
{
  public static final String[] ENGLISH_STOP_WORDS = {
    "a", "and", "are", "as", "at", "be", "but", "by",
    "for", "if", "in", "into", "is", "it",
    "no", "not", "of", "on", "or", "s", "such",
    "t", "that", "the", "their", "then", "there", "these",
    "they", "this", "to", "was", "will", "with"
  };

  public static void main( String[] args )
  {
    try
    {
      HashSet set = new HashSet( Arrays.asList(ENGLISH_STOP_WORDS) );

      String path = "C:/ulli/kurs";

      RandomAccessFile file = new RandomAccessFile( path+"/The Battle Of The Strong, Gilbert Parker.txt","r" );

      String line;
      int count = 0;

      while ( (line = file.readLine()) != null )
      {
        StringTokenizer st = new StringTokenizer( line, " \t\n\r\"\'.,;?!:+-_" );

        while ( st.hasMoreTokens() )
        {
          String tok = st.nextToken().toLowerCase();

          if ( set.contains(tok) )
            continue;

//          System.out.println( tok );

          count++;
        }
      }
      System.out.println( "count = " + count );
    }
    catch ( IOException e )
    {
      e.printStackTrace();
    }
  }
}

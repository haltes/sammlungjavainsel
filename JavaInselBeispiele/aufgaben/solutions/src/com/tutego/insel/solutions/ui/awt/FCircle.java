package com.tutego.insel.solutions.ui.awt;

import java.awt.*;
import java.awt.event.*;

public class FCircle extends Frame {

  private static final long serialVersionUID = -610705626036618898L;

  public FCircle() {
    setSize( 400, 300 );
    setVisible( true );

    addWindowListener( new WindowAdapter() {
      @Override
      public void windowClosing( WindowEvent e ) {
        System.exit( 0 );
      }
    } );
  }

  static void drawPoint( Graphics g, int x, int y ) {
    g.drawLine( x, y, x, y );
  }

  static void fcircle( Graphics g, int x, int y, int radius ) {
    int balance, xoff, yoff;

    xoff = 0;
    yoff = radius;
    balance = -radius;

    do {
      drawPoint( g, x + xoff, y + yoff );
      drawPoint( g, x - xoff, y + yoff );
      drawPoint( g, x - xoff, y - yoff );
      drawPoint( g, x + xoff, y - yoff );
      drawPoint( g, x + yoff, y + xoff );
      drawPoint( g, x - yoff, y + xoff );
      drawPoint( g, x - yoff, y - xoff );
      drawPoint( g, x + yoff, y - xoff );

      if ( (balance += xoff++ + xoff) >= 0 )
        balance -= --yoff + yoff;

    } while ( xoff <= yoff );
  }

  @Override
  public void paint( Graphics g ) {
    fcircle( g, 100, 100, 50 );
  }

  public static void main( String[] args ) {
    new FCircle();
  }
}
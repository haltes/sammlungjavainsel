package com.tutego.insel.solutions.radio.v3;

public class Radio {

  public enum Band { AM, FM }

  private int lautstärke;
  private Band band = Band.AM;
  private double frequenz;
  private boolean eingeschaltet;

  public void setzeBand( Band band ) {
    if ( band == null ) {
      System.err.println( "Band darf nicht null sein!" );
      return;
    }

    this.band = band;
  }

  public Band getBand() {
    return band;
  }

  public double getFrequenz() {
    return frequenz;
  }

  void waehleSender( double newFrequenz ) {
    if ( eingeschaltet )
      frequenz = newFrequenz;
  }

  void volume( int x ) {
    if ( eingeschaltet ) {
      lautstärke += x;
      if ( lautstärke > 100 )
        lautstärke = 100;
      if ( lautstärke < 0 )
        lautstärke = 0;
    }
  }

  void lauter() {
    volume( 1 );
  }

  void leiser() {
    volume( -1 );
  }

  void an() {
    eingeschaltet = true;
  }

  void aus() {
    eingeschaltet = false;
  }

  boolean istAn() {
    return eingeschaltet;
  }

  @Override
  public String toString() {
    return "Radio: Sender = " + frequenz + " Lautstaerke = " + lautstärke + " "
           + (eingeschaltet ? "Radio ist an." : "Radio ist aus." ) +
           " Band: " + band;
  }
}
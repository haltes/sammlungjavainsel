package com.tutego.insel.solutions.lang;

import javax.swing.*;

public class Alternativen {

  public static void main( String[] args ) {
    String s = JOptionPane.showInputDialog( "Bitte Zahl eingeben" );

    if ( Integer.parseInt( s ) < 0 )
      System.out.println( "negativ" );
    else
      System.out.println( "positiv" );

    double x = Math.sin( 0.123456 ); // Zahl ist Überraschung

    if ( (x > 0) && (x < 1) )
      System.out.println( "passt" );

    System.out.println( "Zahl x: " + x );

    System.exit( 0 );
  }
}
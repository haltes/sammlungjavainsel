package com.tutego.insel.solutions.io;

import java.io.*;

public class DataOutputTest {

  public static void main( String[] args ) {
    DataOutputStream dos = null;
    
    try {
      OutputStream fos = new FileOutputStream( "test.txt" );
      dos = new DataOutputStream( fos );

      dos.writeChars( "There cannot be a crisis next week. My schedule is already full." );
      dos.writeChar( 'H' );
      dos.writeUTF( "enry Alfred Kissinger" );
      dos.writeByte( 27 );
      dos.writeShort( 5 );
      dos.writeInt( 1923  );
    }
    catch ( FileNotFoundException e ) { e.printStackTrace(); }
    catch ( IOException e ) { e.printStackTrace(); }
    finally {
      if ( dos != null )
        try {
          dos.close();
        }
        catch ( IOException e ) { e.printStackTrace(); }
    }
  }
}
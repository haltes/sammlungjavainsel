package com.tutego.insel.solutions.io;

import java.io.*;

public class FindFile {

  public static void main( String[] args ) {
    System.out.println( findFile( "C:/", "brndlog.txt" ) );
  }

  public static String findFile( String path, String search ) {
    System.out.println( "Teste Verzeichnis " + path );

    for ( File f : new File( path ).listFiles() ) {
      if ( f.getName().equals( search ) )
        return f.getName();
      else if ( f.isDirectory() ) {
        String find = findFile( f.getAbsolutePath(), search );
        if ( find != null )
          return f.getAbsolutePath() + File.separatorChar + search;
      }
    }
    return null;
  }
}
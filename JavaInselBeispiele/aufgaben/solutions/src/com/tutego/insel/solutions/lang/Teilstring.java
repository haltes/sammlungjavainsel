package com.tutego.insel.solutions.lang;

public class Teilstring {

  public static String replaceString( String src, String search, String replacement ) {
    String result = src;

    for ( int i = src.indexOf( search ); i >= 0; ) {
      result = result.substring( 0, i ) + replacement + result.substring( i + search.length() );

      i = result.indexOf( search );
    }
    return result;
  }

  public static void main( String[] args ) {
    System.out.println( replaceString( "abcdebcdfgbcd", "bcd", "xyz123" ) );
  }
}
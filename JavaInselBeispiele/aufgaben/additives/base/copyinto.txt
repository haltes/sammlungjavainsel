I create a vector which contains objects of type 'SomeClass', and later I
want to have that vector in array form. I found that Vector.elements just
returns an array of type 'Object'. I found that I can't do this: (run-time
exception thrown)
SomeClass[] someArray = (SomeClass[]) someVector.getElements();


Is that true I have to loop through the array 'someclassVector.elements' and
cast every element back into type 'SomeClass'?
or did I miss something or what?

Thanks for any help:)


Antwort:
Vector.elements() returns an Enumeration, not an array (of Object or
otherwise).

You mean to do:

Vector v = new Vector();
SomeClass[] i = new SomeClass[v.size()];
v.copyInto(i);

Now that does simply do a copy one-element-at-a-time. Doing anything
else would mean creating a specialized vector class for your type.
Unless this particular operation is the bottleneck limiting 

package com.tutego.insel.solutions.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CompoundComparator implements Comparator
{
  private final List comparators = new ArrayList();

  public CompoundComparator()
  {
  }

  public CompoundComparator( Comparator comparator1, Comparator comparator2 )
  {
    addComparator( comparator1 );
    addComparator( comparator2 );
  }

  public void addComparator( Comparator comparator )
  {
    if ( comparator == null )
      throw new IllegalArgumentException( "null comparator not allowed!" );

    comparators.add( comparator );
  }

  public int compare( Object o1, Object o2 )
  {
    for ( int i = 0; i < comparators.size(); i++ )
    {
      Comparator comparator = (Comparator) comparators.get( i );

      int result = comparator.compare( o1, o2 );

      if ( result != 0 )
        return result;
    }

    return 0;
  }
}

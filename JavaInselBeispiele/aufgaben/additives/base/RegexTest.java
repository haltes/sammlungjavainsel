import java.util.regex.*;

public class RegexTest
{
  public static void main( String[] args )
  {
    String value;

    // [ABC]
    value = "C";
    //value = "c";
    System.out.println("1  : " + value.matches("[ABC]") );

    // [A-Z]
    value = "T";
    //value = "t";
    System.out.println("2  : " + value.matches("[A-Z]") );

    // [A-Za-z0-9]
    value = "3";
    //value = ".";
    System.out.println("3  : " + value.matches("[A-Za-z0-9]") );

    // [^ABC]
    value = "D";
    //value = "B";
    System.out.println("4  : " + Pattern.matches("[^ABC]", value));

    // [^A-Z]
    value = "c";
    //value = "C";
    System.out.println("5  : " + Pattern.matches("[^A-Z]", value));

    // \w
    value = "9";
    //value = "$";
    System.out.println("6  : " + Pattern.matches("\\w", value));

    // \W
    value = "=";
    //value = "t";
    System.out.println("7  : " + Pattern.matches("\\W", value));

    // \d
    value = "5";
    //value = "k";
    System.out.println("8  : " + Pattern.matches("\\d", value));

    // \D
    value = "k";
    //value = "3";
    System.out.println("9  : " + Pattern.matches("\\D", value));

    // \s
    value = " ";
    //value = "t";
    System.out.println("10 : " + Pattern.matches("\\s", value));

    // \S
    value = "t";
    //value = " ";
    System.out.println("11 : " + Pattern.matches("\\S", value));

    // \n
    value = "\n";
    //value = "t";
    System.out.println("12 : " + Pattern.matches("\\n", value));

    // A+
    value = "AAA";
    //value = "BBB";
    System.out.println("13 : " + Pattern.matches("A+", value));

    // A*
    value = "A";
    //value = "BBBBB";
    System.out.println("14 : " + Pattern.matches("A*", value));

    // A?
    value = "A";
    //value = "AA";
    System.out.println("15 : " + Pattern.matches("A?", value));

    // A{5}
    value = "AAAAA";
    //value = "AAAAAAA";
    System.out.println("16 : " + Pattern.matches("A{5}", value));

    // A{3,}
    value = "AAA";
    //value = "AA";
    System.out.println("17 : " + Pattern.matches("A{3,}", value));

    // A{3,5}
    value = "AAAA";
    //value = "AAAAAA";
    System.out.println("18 : " + Pattern.matches("A{3,5}", value));

    // ^take.*
    value = "takeoba";
    //value = "java";
    System.out.println("19 : " + Pattern.matches("^take.*", value));

    // *.oba$
    value = "takeoba";
    //value = "java";
    System.out.println("20 : " + Pattern.matches(".*oba$", value));
  }
}
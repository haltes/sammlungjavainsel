import java.awt.*;
import java.awt.event.*;

public class MMLBeispiel
{
  public static void main( String args[] )
  {
    Frame f = new Frame();
    f.addMouseMotionListener( new MyMML() );
    f.setSize( 400, 400 );
    f.show();
  }
}

class MyMML implements MouseMotionListener
{
  public void mouseDragged( MouseEvent e )
  {
  }

  public void mouseMoved( MouseEvent e )
  {
    System.out.println( e );
  }
}

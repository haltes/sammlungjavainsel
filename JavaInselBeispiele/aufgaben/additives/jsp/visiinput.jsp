<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Ihre Daten</title>
<style type="text/css">
<!--
body, td, div, p, span {
	font-family: Verdana, Arial, Helvetica, Sans;
	color:#003366;
}
.input {
	border: 1px;
	border-color: #cccccc;
	border-style: solid;
	width: 140px;
	font-size: 10px;
	color: #333333;
	background-color: #ffffff;
}	
.s {
	font-size: 11px;
	color: 666666;
}
-->
</style>
</head>

<body>

<h2>Bitte geben Sie Ihre Daten ein</h2>

<form name="userinput" action="http://localhost:8080/Tommy/test.jsp">
<table border="0" cellspacing="0" cellpadding="0" bgcolor="#EFEFEF">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td class="s">Vorname</td>
    <td class="s">&nbsp;</td>
    <td class="s">Nachname</td>
    <td>&nbsp;</td>
    <td class="s">Textformat</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td><input class="input" size="10" name="forname"></td>
    <td>&nbsp;</td>
    <td><input class="input" size="10" name="surname"></td>
    <td></td>
    <td>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><select style="font-size: 11px" size="1" name="nameFont">
                <option value="American Typewriter">American Typewriter
                <option value="Arial" selected>Arial
                <option value="Arial Black">Arial Black
                <option value="Arial Narrow">Arial Narrow
                <option value="Bank Gothic">Bank Gothic
                <option value="Bodoni">Bodoni
                <option value="Book Antiqua">Book Antiqua
                <option value="Comic">Comic
                <option value="Copperplate Gothic">Copperplate Gothic
                <option value="Dauphin">Dauphin
                <option value="English">English
                <option value="Engravers Gothic">Engravers Gothic
                <option value="Engravers Roman">Engravers Roman
                <option value="Futura">Futura
                <option value="Garamond">Garamond
                <option value="Humanist">Humanist
                <option value="Lucida Console">Lucida Console
                <option value="Microgramma">Microgramma
                <option value="Times">Times
                <option value="Typo Upright">Typo Upright
                <option value="Verdana">Verdana</option>
              </select></td>
          <td><input type="checkbox" name="nameBold" value="ON"></td>
          <td class="s"><b>F</b> </td>
          <td><input type="checkbox" value="true" name="nameItalic"></td>
          <td class="s"><i>K</i>&nbsp;</td>
          <td><select style="FONT-SIZE: 11px" size="1" name="nameFontSize">
                <option value="10" selected>10
                <option value="12">12
                <option value="14">14
                <option value="16">16
                <option value="18">18
                <option value="6">6
                <option value="8">8</option>
              </select></td>
        </tr>
      </table>
    </td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td class="s" >Zusatz</td>
    <td class="s" >&nbsp;</td>
    <td class="s" >&nbsp;</td>
    <td></td>
    <td class="s" >Textformat</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td><input class="input" size="30" name="addition"></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><select style="font-size: 11px" size="1" name="additionFont">
                <option value="American Typewriter">American Typewriter
                <option value="Arial" selected>Arial
                <option value="Arial Black">Arial Black
                <option value="Arial Narrow">Arial Narrow
                <option value="Bank Gothic">Bank Gothic
                <option value="Bodoni">Bodoni
                <option value="Book Antiqua">Book Antiqua
                <option value="Comic">Comic
                <option value="Dauphin">Dauphin
                <option value="English">English
                <option value="Engravers Gothic">Engravers Gothic
                <option value="Engravers Roman">Engravers Roman
                <option value="Futura">Futura
                <option value="Garamond">Garamond
                <option value="Humanist">Humanist
                <option value="Lucida Console">Lucida Console
                <option value="Verdana">Verdana</option>
              </select></td>
          <td><input type="checkbox" value="true" name="additionBold"></td>
          <td class="s"><b>F</b></td>
          <td ><input type="checkbox" value="true" name="additionItalic"></td>
          <td class="s"><i>K</i>&nbsp;</td>
          <td><select style="font-size: 11px" size="1" name="additionFontSize">
                <option value="10" selected>10
                <option value="12">12
                <option value="14">14
                <option value="16">16
                <option value="18">18
                <option value="6">6
                <option value="8">8</option>
              </select></td>
        </tr>
      </table>
    </td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td class="s">Adresse</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td class="s" >Textformat</td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3"><input class="input" size="30" name="address1" style="width: 288"></td>
    <td></td>
    <td>
      <table border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><select style="font-size: 11px" size="1" name="addressFont">
                <option value="American Typewriter">American Typewriter
                <option value="Arial" selected>Arial
                <option value="Arial Black">Arial Black
                <option value="Arial Narrow">Arial Narrow
                <option value="Bank Gothic">Bank Gothic
                <option value="Bodoni">Bodoni
                <option value="Book Antiqua">Book Antiqua
                <option value="Comic">Comic
                <option value="Dauphin">Dauphin
                <option value="English">English
                <option value="Futura">Futura
                <option value="Garamond">Garamond
                <option value="Humanist">Humanist
                <option value="Lucida Console">Lucida Console
                <option value="Times">Times
                <option value="Verdana">Verdana</option>
              </select></td>
          <td><input type="checkbox" value="2" name="addressBold"></td>
          <td class="s"><b>F</b></td>
          <td><input type="checkbox" value="1" name="addressItalic"></td>
          <td class="s"><i>K</i>&nbsp;</td>
          <td><select style="font-size: 11px" size="1" name="addressFontSize">
                <option value="10" selected>10
                <option value="12">12
                <option value="14">14
                <option value="16">16
                <option value="18">18
                <option value="6">6
                <option value="8">8</option>
              </select></td>
        </tr>
      </table>
    </td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3"><input class="input" size="30" name="address2" style="width: 288px"></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3"><input class="input" size="30" name="address3" style="width: 288px"></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="3"><input class="input" size="30" name="address4" style="width: 288px"></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td align="right">
    <a href="javascript:document.userinput.submit();"><sub>Vorschau</sub></a>
    </td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
</form>

<p>&nbsp;</p>

</body>

</html>
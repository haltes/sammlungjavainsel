import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseUtility
{
  /**
   * Database URL
   */
	private String url = "jdbc:postgresql:GeoDaten";

	/**
	 * Username for the database.
	 */
	private String username = "postgres";
	
	/**
	 * Password for the database.
	 */
	private String password = "";
	
	/**
	 * Connection to the database.
	 */
	private Connection connection;
	
	/**
	 * Gets a connection to the database. Loads the driver.
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException
	{
		if ( connection != null )
			return connection; 

		try
    {
			Class.forName( "org.postgresql.Driver" );
    }
    catch ( ClassNotFoundException e )
    {
      e.printStackTrace();
      System.exit( 0 );
    }

		return connection = DriverManager.getConnection( url, username, password );
	}
	
	/**
	 * Gets an new Statement.
	 * 
	 * @return
	 * @throws SQLException
	 */
	public Statement getStatement() throws SQLException
	{
	  if ( connection == null )
	    getConnection();

		return connection.createStatement();
	}
	
	/**
	 * Closes the connection.
	 *
	 */
	public void closeConnection() throws SQLException
	{
	  connection.close();
	}
	
  /**
   * @return Returns the password.
   */
  public String getPassword()
  {
    return password;
  }

  /**
   * @param password The password to set.
   */
  public void setPassword(String password)
  {
    this.password = password;
  }

  /**
   * @return Returns the url.
   */
  public String getUrl()
  {
    return url;
  }

  /**
   * @param url The url to set.
   */
  public void setUrl( String url )
  {
    this.url = url;
  }

  /**
   * @return Returns the username.
   */
  public String getUsername()
  {
    return username;
  }

  /**
   * @param username The username to set.
   */
  public void setUsername(String username)
  {
    this.username = username;
  }

  /**
   * @param connection The connection to set.
   */
  public void setConnection(Connection connection)
  {
    this.connection = connection;
  }

}

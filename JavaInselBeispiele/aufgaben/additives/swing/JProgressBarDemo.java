import javax.swing.*;

public class JProgressBarDemo
{
  public static void main( String args[] )
  {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    
    final int max = 10;
    
    final JProgressBar bar = new JProgressBar( 0, max );
    frame.getContentPane().add( bar );
    
    frame.pack();
    frame.show();
    
    // Anzeige in Veränderung
    
    for ( int i = 1; i <= max; i++ )
    {
      try { Thread.sleep( 1500 ); } catch ( InterruptedException e ) { }
      
      final int j = i;
      SwingUtilities.invokeLater( new Runnable() {
            public void run() { bar.setValue( j ); }
          } );
    }
  }
}

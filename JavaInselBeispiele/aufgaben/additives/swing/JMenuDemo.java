import javax.swing.*;
import java.awt.*;

class InternSpaceIcon implements Icon
{
  static InternSpaceIcon icon = new InternSpaceIcon();
  
  public int getIconHeight()   {   return 1;   }
  
  public  int getIconWidth()  {    return 16; }
  
  public void paintIcon( Component c, Graphics g, int x, int y )
  {
  }
}

public class JMenuDemo
{
  public static void main(String[] args)
  {
    JFrame f = new JFrame();

    Container container = f.getContentPane();

    container.add( new Label("Hallo Welt!") );

    JMenuBar mb = new JMenuBar();
    JMenu menu = new JMenu();

    menu.add( new JMenuItem( "Label1" ) );
    menu.add( new JMenuItem( "Label2", new ImageIcon("http://tutego.de/java/aufgaben/insel/images/walkingMen.gif") ) );
    menu.add( new JMenuItem( "Label3", InternSpaceIcon.icon ) );
    menu.add( new JMenuItem( "Label4", InternSpaceIcon.icon ) );
    mb.add( menu );
    
    f.setJMenuBar( mb );

    f.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    f.setSize( 200, 300 );
    f.setVisible( true );
  }
}

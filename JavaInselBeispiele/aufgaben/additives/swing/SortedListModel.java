import javax.swing.*;
import java.util.*;

public class SortedListModel extends AbstractListModel
{
  XXXSet model;

  public SortedListModel() {
     ...
  }

  public int getSize() {
    ...
  }

  public Object getElementAt(int index) {
    ...
  }

  public void add(Object element) {
    if (model.add(element)) {
      fireContentsChanged(this, 0, getSize());
    }
  }

  public void addAll(Object elements[]) {
    Collection c = Arrays.asList(elements);
    model.addAll(c);
    fireContentsChanged(this, 0, getSize());
  }

  public void clear() {
    model.clear();
    fireContentsChanged(this, 0, getSize());
  }

  public boolean contains(Object element) {
   ...
  }

  public Object firstElement() {
    ...
  }

  public Iterator iterator() {
    return model.iterator();
  }

  public Object lastElement() {
    ...
  }

  public boolean removeElement(Object element) {
    boolean removed = model.remove(element);
    if (removed) {
      fireContentsChanged(this, 0, getSize());
    }
    return removed;
  }
}

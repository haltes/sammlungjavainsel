import java.awt.*;
import java.awt.event.*;

class InfoFrame extends Frame
{
  InfoFrame()
  {
    // Layout
    
    setLayout( new BorderLayout() );
    
    final Label pic = new Label( "Bild" );
    final Label info = new Label( "Info" );
    
    add( pic, BorderLayout.NORTH );
    add( info, BorderLayout.CENTER );

    Panel okpanel = new Panel();
    okpanel.setLayout( new FlowLayout() );

    final Button ok = new Button( "OK" );
    okpanel.add( ok );
    
    add( okpanel, BorderLayout.SOUTH );
    
    pack();
    
    // Ereignisbehandlung
    
    ok.addActionListener( new ActionListener()
    {
      public void actionPerformed( ActionEvent e )
      {
        dispose();
        System.exit(0);
      }
    } );
  }
}

public class SwingCourseMain
{
  public static void main( String args[] )
  {
    Frame info = new InfoFrame();
    info.setVisible( true );
  }
}

public class LengthAndEmptyDemo {
  
  /**
   * Checks if a String is {@code null} or empty ({@code ""}).
   *
   * <pre>
   * StringUtils.isEmpty(null) == true
   * StringUtils.isEmpty(&quot;&quot;) == true
   * StringUtils.isEmpty(&quot; &quot;) == false
   * StringUtils.isEmpty(&quot;bob&quot;) == false
   * StringUtils.isEmpty(&quot; bob &quot;) == false
   * </pre>
   *
   * @param str The String to check, may be {@code null}.
   * @return {@code true} if the String is empty or {@code null}, {@code false} otherwise.
   */
  public static boolean isNullOrEmpty( String str ) {
    return str == null || str.isEmpty();
  }

}

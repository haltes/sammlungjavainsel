package com.tutego.insel.io.stream;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Date;

public class WriteInFile {
  public static void main( String[] args ) {
    try ( PrintWriter out = new PrintWriter( "c:/datei.txt", StandardCharsets.ISO_8859_1.name() ) ) {
      out.println( "Hallo Welt!" );
      out.print( "Es ist " );
      out.printf( "%tT Uhr.", new Date() );
    }
    catch ( FileNotFoundException | UnsupportedEncodingException e ) {
      e.printStackTrace();
    }
  }
}
package com.tutego.insel.io.ser;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class DeSerializeNameAndDate {
  public static void main( String[] args ) {
    try ( InputStream fis = Files.newInputStream( Paths.get( "name_date.ser" ) );
          ObjectInputStream ois = new ObjectInputStream( fis ) ) {
      String name = (String) ois.readObject();
      Date date = (Date) ois.readObject();

      System.out.println( name );
      System.out.println( date );
    }
    catch ( IOException | ClassNotFoundException e ) {
      e.printStackTrace();
    }
  }
}
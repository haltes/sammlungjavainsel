package com.tutego.insel.io.ser;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class SerializeNameAndDate {
  public static void main( String[] args ) {
    try ( OutputStream fos = Files.newOutputStream( Paths.get( "name_date.ser" ) );
          ObjectOutputStream oos = new ObjectOutputStream( fos ) ) {
      oos.writeObject( "Chris" );
      oos.writeObject( new Date() );
    }
    catch ( IOException e ) {
      System.err.println( e );
    }
  }
}
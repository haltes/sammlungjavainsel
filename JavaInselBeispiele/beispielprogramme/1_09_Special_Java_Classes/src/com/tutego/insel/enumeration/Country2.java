package com.tutego.insel.enumeration;

import java.util.Locale;

public enum Country2 {

  GERMANY( Locale.GERMANY ), UK( Locale.UK ), CHINA( Locale.CHINA );

  private Locale country;

  private Country2( Locale country ) {
    this.country = country;
  }

  public String getISO3Country() {
    return country.getISO3Country();
  }
}
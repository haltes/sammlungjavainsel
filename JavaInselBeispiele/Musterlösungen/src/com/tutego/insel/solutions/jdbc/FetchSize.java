package com.tutego.insel.solutions.jdbc;

import java.sql.*;

public class FetchSize
{
  public static void main( String[] args ) throws SQLException
  {
    DatabaseUtility db = new DatabaseUtility();

    Statement stmt = db.getStatement();
    
    int fetchSize = stmt.getFetchSize();
    System.out.println( fetchSize );
    
    stmt.setFetchSize( 100 );
    
    ResultSet rs = stmt.executeQuery( "SELECT * FROM geodb_adm0" );
  
    while ( rs.next() )
    {
      System.out.print(   rs.getString( "adm0" ) + ", " );
      System.out.println( rs.getString( "name" ) );
    }

    rs.close();
    stmt.close();
  }
}

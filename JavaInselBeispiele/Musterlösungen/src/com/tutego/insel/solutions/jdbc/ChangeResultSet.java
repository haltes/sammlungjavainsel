package com.tutego.insel.solutions.jdbc;
import java.sql.*;

public class ChangeResultSet
{
  public static void main( String[] args )
  {
    DatabaseUtility dbUtil = new DatabaseUtility();

    try ( Connection con = dbUtil.getConnection() ) {
      Statement statement = con.createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE, 
                                                 ResultSet.CONCUR_UPDATABLE );

      String query = "SELECT * FROM geodb_adm1 where adm0 = 'DE'";
      ResultSet rs = statement.executeQuery( query );

      while ( rs.next() )
      {  
        rs.updateString( "adm0", "de" );
        rs.updateRow();
      }
    
      rs.close();      
      statement.close();
    }
    catch ( SQLException e )
    {
      e.printStackTrace();
    }
  }
}

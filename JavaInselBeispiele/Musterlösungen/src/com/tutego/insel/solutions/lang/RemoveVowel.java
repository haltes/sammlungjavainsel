package com.tutego.insel.solutions.lang;

public class RemoveVowel {

  public static void main( String[] args ) {
    System.out.println( removeVocals( "Hallo Javanesen" ) );
  }

  static String removeVocals( String s ) {
    char[] chars = new char[ s.length() ];
    int j = 0;

    for ( int i = 0; i < s.length(); ++i ) {
      char c = s.charAt( i );

      if ( "aeiouAEIOU".indexOf( c ) >= 0 )
        continue;

      chars[ j++ ] = c;
    }

    return new String( chars, 0, j );
  }
}

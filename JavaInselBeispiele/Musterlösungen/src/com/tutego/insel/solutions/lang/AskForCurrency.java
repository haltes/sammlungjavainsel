package com.tutego.insel.solutions.lang;

import javax.swing.JOptionPane;

public class AskForCurrency
{
  enum Currency
  {
    EUR, USD
  }

  public static void main( String[] args )
  {
    String input = JOptionPane.showInputDialog( null, "Bitte W�hrung eingeben" );

    try
    {
      Currency currency = Currency.valueOf( input.toUpperCase() );

      switch ( currency )
      {
        case EUR:
          System.out.println( "EUR" );
          break;
          
        case USD:
          System.out.println( "USD" );
          break;
          
        default :
          System.out.println( "Bekannte W�hrung nicht verarbeitet." );
      }
    }
    catch ( IllegalArgumentException e )
    {
      System.out.println( "Die Eingabe ist ung�ltig!" );
    }
  }
}
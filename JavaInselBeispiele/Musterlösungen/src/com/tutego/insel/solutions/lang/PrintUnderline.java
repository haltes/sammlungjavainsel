package com.tutego.insel.solutions.lang;

public class PrintUnderline {

  public static void main( String[] args ) {
    System.out.println( args[ 0 ] );

    underline( args[ 0 ], args[ 1 ] );
  }

  /**
   * @param origLine
   * @param searchWord
   */
  private static void underline( String origLine, String searchWord ) {
    int i = 0;
    int last = 0;

    while ( i >= 0 ) {
      i = origLine.indexOf( searchWord, i );

      if ( i < 0 )
        break;

      outputChar( i, last, ' ' );
      outputChar( searchWord.length(), 0, '+' );
      last = i + searchWord.length();

      i++;
    }
  }

  /**
   * @param i
   * @param last
   * @param c
   */
  private static void outputChar( int i, int last, char c ) {
    for ( int j = last; j < i; j++ )
      System.out.print( c );
  }
}

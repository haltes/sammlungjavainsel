package com.tutego.insel.solutions.xml;

import org.jdom.*;
import org.jdom.output.*;

public class JDOMDemo
{
  public static void main( String[] args ) throws Exception
  {
    Element root = new Element( "root" ).setText( "This is the root" );
    Element innen = new Element( "innen" ).setText( "hier ist Innen" );
    root.addContent( new Element( "innen" ).setText( "hier ist Innen" ) .setAttribute( "vspace", "0" ) );

    root.addContent( innen );
    root.addContent( new Element( "innen" ).setText( "hier ist Innen" ) );
    root.addContent( new Element( "innen" ).setText( "hier ist Innen" ) );

    CDATA data = new CDATA( "<xml> content" );
    innen.addContent( data );

    Namespace xhtml = Namespace.getNamespace( "xhtml",
                                              "http://www.w3.org/1999/xhtml" );

    root.addContent( new Element( "table", xhtml ) );

    Document doc = new Document( root );

    XMLOutputter outp = new XMLOutputter();
    outp.getFormat().setIndent( " " );
    outp.getFormat().setLineSeparator( "\n" );
    outp.output( doc, System.out );
  }
}
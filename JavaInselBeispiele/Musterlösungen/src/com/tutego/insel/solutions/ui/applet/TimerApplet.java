package com.tutego.insel.solutions.ui.applet;

import java.applet.*;
import java.awt.*;
import java.util.*;

public class TimerApplet extends Applet {

  private static final long serialVersionUID = 1L;

  @Override
  public void paint( Graphics g ) {
    Font f = new Font( "Tahoma", Font.PLAIN, 25 );
    g.setColor( Color.YELLOW );
    g.fillRect( 0, 0, getWidth() - 1, getHeight() - 1 );
    g.setColor( Color.BLUE );
    g.setFont( f );
    g.drawString( "Time: " + getTime(), 60, 100 );
    repaint( 1000 );
  }

  @SuppressWarnings( "deprecation" )
  private String getTime() {
    Date d = new Date();
    return d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
  }
}

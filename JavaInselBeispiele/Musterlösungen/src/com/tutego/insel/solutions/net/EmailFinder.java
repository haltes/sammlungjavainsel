package com.tutego.insel.solutions.net;

import java.io.*;
import java.net.URL;

public class EmailFinder {

  public static void main( String[] args ) throws Exception {
    // Properties prop =System.getProperties();

    // prop.put("proxyHost","proxy.wincor-nixdorf.com");
    // prop.put("proxySet","true");
    // prop.put("proxyPort","81");

    URL url = new URL( "http://www.galileocomputing.de/hilfe/Impressum" );

    InputStream in = url.openStream();
    BufferedReader br = new BufferedReader( new InputStreamReader( in ) );

    for ( String line; (line = br.readLine()) != null; ) {
      int mtpos = line.indexOf( "mailto:" );

      if ( mtpos >= 0 ) {
        int epos = line.indexOf( "\"", mtpos + "mailto:".length() );

        if ( epos >= 0 )
          System.out.println( line.substring( mtpos, epos ) );
      }
    }

    br.close();
  }
}

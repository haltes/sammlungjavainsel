package com.tutego.insel.solutions.thread;

import java.io.*;

public class DateiBeobachter implements Runnable {

  private File file;
  private Runnable run;

  public DateiBeobachter( String filename, Runnable r ) {
    this( new File( filename ), r );
  }

  public DateiBeobachter( File filename, Runnable r ) {
    file = filename;
    run = r;
  }

  @Override
  public void run() {
    long last = file.lastModified();

    while ( true ) {
      long newlastmod = file.lastModified();

      if ( last < newlastmod ) {
        last = newlastmod;
        run.run();
      }

      try {
        Thread.sleep( 500 );
      }
      catch ( InterruptedException e ) {
        throw new RuntimeException( e.getMessage(), e );
      }
    }
  }

  public static void main( String[] args ) {
    String filename = "c:/yes.txt";

    DateiBeobachter db = new DateiBeobachter( filename, new InformMe() );

    Thread t = new Thread( db );
    t.start();
  }
}

class InformMe implements Runnable {

  @Override
  public void run() {
    System.out.println( "Geändert. Toll gemacht!" );
  }
}
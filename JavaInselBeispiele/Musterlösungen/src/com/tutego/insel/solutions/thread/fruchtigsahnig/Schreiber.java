package com.tutego.insel.solutions.thread.fruchtigsahnig;

public class Schreiber {

  public synchronized void schreibe( String str ) {
    try {
      notify();
      System.out.println( str );
      wait();
    }
    catch ( Exception e ) {
      e.printStackTrace();
    }
  }
}
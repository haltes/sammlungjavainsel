package com.tutego.insel.solutions.thread;

import javax.swing.*;
import java.awt.event.*;

public class Uhrzeit extends JFrame {
  
  private static final long serialVersionUID = -9215935690459812855L;

  public Uhrzeit() {
    getContentPane().add( new TimeLabel() );

    addWindowListener( new WindowAdapter() {
      @Override
      public void windowClosing( WindowEvent e ) {
        System.exit( 0 );
      }
    } );
    pack();
    setVisible( true );
  }

  public static void main( String[] args ) {
    new Uhrzeit();
  }
}
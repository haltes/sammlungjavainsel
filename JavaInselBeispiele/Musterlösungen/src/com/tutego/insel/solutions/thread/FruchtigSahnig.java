package com.tutego.insel.solutions.thread;

public class FruchtigSahnig {
  
  public static void main( String[] args ) {
    Ausgabe t1 = new Ausgabe( "fruchtig" );
    Ausgabe t2 = new Ausgabe( "sahnig" );
    t1.start();
    t2.start();
  }

  static class Ausgabe extends Thread {
    static Object waiter = new Object();

    String message;

    Ausgabe( String s ) {
      message = s;
    }

    @Override
    public void run() {
      while ( true ) {
        synchronized ( waiter ) {
          try {
            waiter.notify();

            System.out.println( message );

            try {
              Thread.sleep( (int) (2000 * Math.random()) );
            }
            catch ( Exception e ) {
            }

            waiter.wait();

          }
          catch ( InterruptedException e ) {
          }
        }
      }
    }
  }
}
package com.tutego.insel.solutions.thread;

import java.util.Date;
import javax.swing.JLabel;

@SuppressWarnings( "deprecation" )
public class TimeLabel extends JLabel implements Runnable {

  private static final long serialVersionUID = 1740575839757428005L;

  TimeLabel() {
    super( new Date().toGMTString() );

    new Thread( this ).start(); // Thread startet sich selbst
  }

  @Override
  public void run() {
    while ( true )
      setText( new Date().toGMTString() );
  }
}
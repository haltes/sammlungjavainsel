package com.tutego.insel.solutions.jsp;
import java.sql.*;

public class NorthwindJdbc
{
  public static void main(String[] args) throws Exception
  {
    Class.forName( "sun.jdbc.odbc.JdbcOdbcDriver" );
    try ( Connection con = DriverManager.getConnection(
                       "jdbc:odbc:Northwind" ) ) {
      Statement statement = con.createStatement();
      String query =
          "SELECT firstname, lastname FROM employees";
      ResultSet set = statement.executeQuery( query );
      while ( set.next() )
        System.out.print( set.getString(1) + " " +
                          set.getString(2) );
    }
  }
}

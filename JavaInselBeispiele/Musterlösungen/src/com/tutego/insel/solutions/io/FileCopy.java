package com.tutego.insel.solutions.io;

import java.io.*;

public class FileCopy {

  public static void main( String[] args ) {
    // System.out.println( fileCopy( "c:/test.txt", "c:/blub.txt" ) );

    insertFile( "c:/test.txt" );
  }

  public static void insertFile( String sourceFile ) {
    File src = new File( sourceFile );

    String path = src.getParent(), name = src.getName();

    File fcopy = new File( path, "Kopie von " + name );

    // gibt's fcopy schon?

    int i = 2;

    while ( fcopy.exists() )
      fcopy = new File( path, "Kopie (" + i++ + ") von " + name );

    copyFile( src, fcopy );
  }

  public static boolean copyFile( File source, File dest ) {
    InputStream  in  = null;
    OutputStream out = null;
    try {
      in  = new FileInputStream( source );
      out = new FileOutputStream( dest );

      byte[] buffer = new byte[4096];

      int len;
      while ( (len = in.read( buffer )) > 0 )
        out.write( buffer, 0, len );
    }
    catch ( IOException e ) {
      return false;
    }
    finally {
      if ( out != null )
        try { out.close(); }
        catch ( IOException e ) { e.printStackTrace(); }
      if ( in != null )
        try { in.close(); }
        catch ( IOException e ) { e.printStackTrace(); }
    }
    return true;
  }

  public static boolean fileCopy( String source, String dest ) {
    return copyFile( new File( source ), new File( dest ) );
  }
}
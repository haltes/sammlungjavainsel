package com.tutego.insel.solutions.io;

import java.io.*;

public class WriteInFile {

  public static void main( String[] args ) {
    try {
      FileOutputStream out = new FileOutputStream( "c:/ABC.txt" );

      // Variante 1

      out.write( 'A' );
      out.write( 'B' );
      out.write( 'C' );

      // Variante 2

      byte[] b = "ABC".getBytes("utf-8");  // besser als "ABC".getBytes();  

      out.write( b );

      out.close();  // sollte in den finally-block
    }
    catch ( IOException e ) {
      e.printStackTrace();
    }
  }
}
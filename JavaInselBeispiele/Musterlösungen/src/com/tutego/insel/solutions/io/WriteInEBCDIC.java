package com.tutego.insel.solutions.io;

import java.io.*;

public class WriteInEBCDIC
{
  public static void main( String[] args )
  {
    PrintWriter out = null;
    try
    {
      OutputStream fos = new FileOutputStream( "streams.txt" );
      OutputStreamWriter osw = new OutputStreamWriter( fos, "Cp500" );
      out = new PrintWriter( osw );

      out.print( "Hallo" );
    }
    catch ( IOException e )
    {
      e.printStackTrace();
    }
    finally
    {
      if ( out != null )
        out.close();
    }
  }
}
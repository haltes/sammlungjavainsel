package com.tutego.insel.solutions.io;

import java.io.*;
import java.util.*;
import java.text.*;

public class FileLister {

  private List<String> files = new ArrayList<>();
  private List<String> dirs = new ArrayList<>();

  public static void main( String[] args ) {
    FileLister fw = new FileLister();
    File file = new File( "c:/" );
    fw.readDir( file );
    fw.showContent();
  }

  private void readDir( File path ) {
    MessageFormat mf = new MessageFormat( "{0}\t\t{1}\t\t{2}" );

    for ( File f : path.listFiles() ) {
      Date d = new Date( f.lastModified() );

      if ( f.isFile() )
        files.add( mf.format( new Object[]{ f.getName(), d, Long.valueOf( f.length() ) } ) );
      else
        dirs.add( "[" + f.getName() + "]" + formatTime( d.getTime() ) );
    }
  }

  private void showContent() {
    for ( String dir : dirs )
      System.out.println( dir );

    for ( String file : files )
      System.out.println( file );
  }

  private String formatTime( long l ) {
    SimpleDateFormat sdf = new SimpleDateFormat( "'\n\t''--->\tGeaendert am' dd.MM.yyyy 'um' HH:mm:ss'Uhr' (z)" );
    return sdf.format( new Date( l ) );
  }
}
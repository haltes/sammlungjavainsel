package com.tutego.insel.solutions.io;

import java.io.*;
import java.util.*;
import java.util.zip.*;

public class ObjectReader {

  public static void main( String[] args ) throws Exception {
    HashMap<String, Integer> hm1 = new HashMap<>();
    hm1.put( "Eins", 1 );
    hm1.put( "Zwei", 2 );
    hm1.put( "Drei", 3 );

    OutputStream fos = new FileOutputStream( "hash.ser" );
    OutputStream bos = new BufferedOutputStream( fos );
    OutputStream gzos = new GZIPOutputStream( bos );
    ObjectOutputStream oos = new ObjectOutputStream( gzos );

    oos.writeObject( hm1 );

    oos.close();

    // *************************************************

    InputStream fis = new FileInputStream( "hash.ser" );
    InputStream bis = new BufferedInputStream( fis );
    InputStream gzis = new GZIPInputStream( bis );
    ObjectInputStream ois = new ObjectInputStream( gzis );

    @SuppressWarnings( "unchecked" )
    HashMap<String, Integer> hm2 = (HashMap<String, Integer>) ois.readObject();

    ois.close();

    System.out.println( hm1 );
    System.out.println( hm2 );
  }
}
package com.tutego.insel.solutions.optim;
import java.util.StringTokenizer;

public class SplitPerformance
{
  static void splitTest1( String s )
  {
    StringTokenizer st = new StringTokenizer( s );

    for ( int len = 0; st.hasMoreTokens(); )
      len += st.nextToken().length();
  }
  
  static void splitTest2( String s )
  {
    String[] tokens = s.split( "\\s" );

    for ( int i = 0, len = 0; i < tokens.length; i++ )
    {
      len += tokens[i].length();
    }
  }
  
  public static void main( String[] args )
  {
    StringBuffer sb = new StringBuffer();
    for ( int i = 0; i < 10000; sb.append("Hallo Welt\n").append(i++) );
    String s = sb.toString();
    splitTest1( s );
    splitTest2( s );
  }
}

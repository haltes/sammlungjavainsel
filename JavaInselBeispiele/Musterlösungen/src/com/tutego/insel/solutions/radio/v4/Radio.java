package com.tutego.insel.solutions.radio.v4;

public class Radio extends Konsumgeraet {

  public static final int FM = 1;
  public static final int AM = 0;

  private int band = FM;
  private double frequenz = 0.0;

  public void setzeBand( int band ) {
    if ( (band == FM) || (band == AM) )
      this.band = band;
    else
      System.out.println( "Falsches Band !" );
  }

  public int getBand() {
    return band;
  }

  public double getFrequenz() {
    return frequenz;
  }

  void waehleSender( double newFrequenz ) {
    if ( eingeschaltet )
      frequenz = newFrequenz;
  }

  @Override
  public String toString() {
    return "Radio: Sender = " + frequenz + " Lautstaerke = " + lautstärke + " "
           + (eingeschaltet ? "Radio ist an." : "Radio ist aus.");
  }

  @Override
  public void aus() {
    super.aus();
    volume( -100 );
    System.out.println( "Das Radio ist augeschaltet. Die Lautst�rke ist Null" );
  }
}
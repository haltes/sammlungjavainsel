package com.tutego.insel.solutions.radio.v5;

class Bildröhre {
  private boolean eingeschaltet;

  public void an() {
    eingeschaltet = true;
    System.out.println( "Bildröhre ist an." );
  }

  public void aus() {
    eingeschaltet = false;
    System.out.println( "Bildröhre ist aus." );
  }

  @Override
  public String toString() {
    return String.format( "Bildröhre [eingeschaltet=%s]", eingeschaltet );
  }
}

class Fernseher {

  private int programm;
  private boolean eingeschaltet;
  private Bildröhre röhre = new Bildröhre();

  public void setzteProgramm( int programm ) {
    this.programm = programm;
  }

  public void an() {
    eingeschaltet = true;
    System.out.println( "TV ist an." );
    röhre.an();
  }

  public void aus() {
    eingeschaltet = false;
    System.out.println( "TV ist aus." );
    röhre.aus();
  }

  @Override
  public String toString() {
    return String.format( "Fernseher [programm=%s, eingeschaltet=%s]", programm, eingeschaltet );
  }
}

public class HausMitFernsehenUndBildroehre {

  public static void main( String[] args ) {
    Fernseher tv = new Fernseher();
    tv.setzteProgramm( 21 );
    tv.an();
    tv.aus();
  }
}

package com.tutego.insel.solutions.radio;

class Radiostationen
{
  public static final String BFBS_STATION_NAME = "bfbs";
  public static final double BFBS_FREQUENZ = 103.;
}

class Radio {

  public static double toFreq( String frequenz ) {

    if ( frequenz == null )
      return 0.0;

    switch ( frequenz.trim().toLowerCase() ) {

      case "wdr 1":
        return 101.;

      case Radiostationen.BFBS_STATION_NAME:
        return Radiostationen.BFBS_FREQUENZ;
        
      default:
        return 0.;
    }
  }
}

public class HausMitRadioToFreq {

  public static void main( String[] args ) {
    System.out.println( Radio.toFreq( "BFBS" ) ); // 103.0
  }

}

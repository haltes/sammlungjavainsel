package com.tutego.insel.solutions.radio.v2;

public class Radio {

  int lautstärke = 0;
  double frequenz = 0.0;
  boolean eingeschaltet = false;

  void waehleSender( double newFrequenz ) {
    if ( eingeschaltet )
      frequenz = newFrequenz;
  }

  void volume( int x ) {
    if ( eingeschaltet ) {
      lautstärke += x;
      if ( lautstärke > 100 )
        lautstärke = 100;
      if ( lautstärke < 0 )
        lautstärke = 0;
    }
  }

  void lauter() {
    volume( 1 );
  }

  void leiser() {
    volume( -1 );
  }

  void an() {
    eingeschaltet = true;
  }

  void aus() {
    eingeschaltet = false;
  }

  boolean istAn() {
    return eingeschaltet;
  }

  @Override
  public String toString() {
    return "Radio: Sender = " + frequenz + " Lautstaerke = " + lautstärke + " "
           + (eingeschaltet ? "Radio ist an." : "Radio ist aus.");
  }
}

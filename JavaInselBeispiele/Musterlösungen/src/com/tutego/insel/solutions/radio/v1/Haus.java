package com.tutego.insel.solutions.radio.v1;

public class Haus {

  public static void main( String[] args ) {
    Radio omisAltesRadio = new Radio();
    omisAltesRadio.eingeschaltet = true;
    omisAltesRadio.lautstärke = 12;

    System.out.println( "So laut: " + omisAltesRadio.lautstärke );
  }
}
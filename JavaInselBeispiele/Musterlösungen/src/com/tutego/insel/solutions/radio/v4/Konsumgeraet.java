package com.tutego.insel.solutions.radio.v4;

public class Konsumgeraet {

  int lautstärke = 0;
  boolean eingeschaltet = false;

  void volume( int x ) {
    if ( eingeschaltet ) {
      lautstärke += x;
      if ( lautstärke > 100 )
        lautstärke = 100;
      if ( lautstärke < 0 )
        lautstärke = 0;
    }
  }

  void lauter() {
    volume( 1 );
  }

  void leiser() {
    volume( -1 );
  }

  void an() {
    eingeschaltet = true;
  }

  void aus() {
    eingeschaltet = false;
  }

  boolean istAn() {
    return eingeschaltet;
  }

  @Override
  public String toString() {
    return "Konsumgerät: Lautst�rke = " + lautstärke + " "
           + (eingeschaltet ? "Konsumgerät ist an." : "Konsumger�t ist aus.");
  }
}
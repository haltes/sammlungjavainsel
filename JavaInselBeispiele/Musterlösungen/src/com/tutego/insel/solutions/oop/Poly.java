package com.tutego.insel.solutions.oop;

import java.awt.Polygon;

public class Poly {

  public static void main( String[] args ) {
    Polygon p = new Polygon();
    
    p.addPoint( 20, 60 );
    p.addPoint( 50, 20 );
    p.addPoint( 80, 60 );

    System.out.println( p.contains( 51, 25 ) );

    for ( int x = 0; x < 100; x++ ) {
      for ( int y = 0; y < 100; y++ )
        System.out.print( p.contains( x, y ) ? "*" : " " );
      System.out.println();
    }
  }
}

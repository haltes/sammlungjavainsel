package com.tutego.insel.solutions.oop.form;

import java.util.ArrayList;

public class FormContainer {

  private ArrayList<Form> formen = new ArrayList<>();

  public void add( Form form ) {
    formen.add( form );
  }

  public double fläche() {
    double summe = 0;

    for ( Form form : formen )
      summe += form.fläche();

    return summe;
  }
}

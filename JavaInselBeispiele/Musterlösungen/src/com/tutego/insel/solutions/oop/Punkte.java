package com.tutego.insel.solutions.oop;

import java.awt.*;

public class Punkte {
  
  public static void main( String[] args ) {
    // Lösung 1
    Point p = new Point();
    p.setLocation( 12, 34 );

    // Lösung 2
    p.x = 12;
    p.y = 34;

    // Lösung 3
    p.move( 12, 34 );

    // Lösung 4
    Point q = new Point();
    q.translate( 12, 34 );

    // Lösung 5
    Point r = new Point();
    r.setLocation( p );

    // Lösung 6
    Point s = new Point( 12, 34 );

    // Lösung 7
    Point t = new Point( p );

    System.out.println( p );
    System.out.println( q );
    System.out.println( r );
    System.out.println( s );
    System.out.println( t );
  }
}